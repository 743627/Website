/*
  ---Logic---
  If name is aldready existing then don't modify name and add to effects
  If name is not existing modify name

*/

/* ---Variables--- */
var texts = ["The Innovator", "The Designer", "The Creator", "The Developer"]
let charCount = 0
let index = 0
const speed = 150
var containers = [document.querySelector('.name'), document.querySelector('.name_cont div')]

/* ---Type text--- */
function type() {
  /* ---Determine if name has to be written--- */
  if(!containers[0].textContent.includes("Rahul Venkateshan")){
    if(charCount < ("Rahul Venkateshan").length){
      containers[0].textContent += ("Rahul Venkateshan")[charCount]
      setTimeout(type, speed)
      if(charCount == ("Rahul Venkateshan").length - 1){
        charCount = 0
      }
    }
  }
  else{
    if(charCount <= texts[index%texts.length].length){
      containers[1].textContent += texts[index%texts.length][charCount-1]
      if(charCount == texts[index%texts.length].length){
        setTimeout(earse, 2500)
      }
      setTimeout(type, speed)
    }
  }
  /* ---Main--- */
  charCount++
}

/* ---Delete text--- */
function earse(){
    if(charCount >= 0 && texts[index%texts.length][charCount]){
      containers[1].textContent = containers[1].textContent.substring(0, charCount)
      if(containers[1].textContent.substring(0, charCount) == ''){
        index++
        charCount = 1
        setTimeout(type, 500)
        return
      }
    }
    setTimeout(earse, 100)
  charCount--
}

/* ---Main driver--- */
window.addEventListener("load", function() {
  type()
}, false);
