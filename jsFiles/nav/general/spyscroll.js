/* Determing scroll bar percentage
  cite: https://stackoverflow.com/questions/2387136/cross-browser-method-to-determine-vertical-scroll-percentage-in-javascript
  Just changed document to body as it makes more sense to me
*/
var line = document.querySelector('hr')
var dots = document.querySelectorAll(".navCol div")

const percent_to_name = {
  '0':'home_r',
  '1':'about_r',
  '2':'skills_r',
  '3':'projects_r'
}

const name_to_percent = {
  'home_r':'0%',
  'about_r':'25%',
  'skills_r':'50%',
  'projects_r':'75%'

}

function scrollToPerctange(){
  var h = document.documentElement,
      b = document.body,
      st = 'scrollTop',
      sh = 'scrollHeight';
      return((Math.round(((h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100)/33)).toString())
}

function spyscroll(){
    var scrollPercent = scrollToPerctange()
    line.style.left = name_to_percent[percent_to_name[scrollPercent]]

  dots.forEach((item, i) => {
    if(i != scrollPercent*1){
      dots[i].style.removeProperty('background-color')
      dots[i].style.removeProperty('transform')
    }else{
      dots[i].style.backgroundColor = "#cb2d3e";
      dots[i].style.transform = "scale(1.5)";
    }
  });
}
